﻿import os

from flask import Flask, render_template

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = 'dev',
        DATABASE = os.path.join(app.instance_path, 'flaskr.sqlite')
    )

    if test_config is None:
        # Load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # closes db and create db-init CLI
    from . import db
    db.init_app(app)

    # auth blueprint
    from . import auth
    app.register_blueprint(auth.bp)

    # blog blueprint
    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    @app.route('/hello')
    def hello_world():
        return 'Hello, World!'

    # my 404 error
    @app.errorhandler(404)
    def page_not_found(e):
        # note that we set the 404 status explicitly
        return '<center><h1>Mi dispiace ma hai cannato l\'url!</h1><br><img src="https://us.123rf.com/450wm/dimarik16/dimarik161801/dimarik16180100397/94571459-sad-beagle-dog-portrait-tired-beagle-dog-close-up-.jpg?ver=6"><br><br>' + str(e)

    return app